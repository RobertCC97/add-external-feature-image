=== Plugin Name ===
Tags: featured image, post, image
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This Plugin is designed to allow you to include an external featured image in your posts

== Description ==

The purpose of this plugin is to take the first image of your posts and turn them into the featured image. If you have a featured image already on a post it will not be overwritten.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin zip file to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Will this overwrite my current featured images =

No.

= Will this change all previous posts that I have created =

No, But in future updates you will have the option to go through and auto update all your posts to include this


== Changelog ==

